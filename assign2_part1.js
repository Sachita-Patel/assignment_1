function changePara1()
{
document.getElementById("para1").innerHTML="New Text!";
}
function insertCurrentDate()
{
document.getElementById("datePara").innerHTML=Date();
}
function originalPara1()
{
  document.getElementById("para1").innerHTML="Original Text";
}

//Bonus Exercise Question : Change style Of Paragraph

window.onload = function() {
  document.getElementById("styledPara").addEventListener("mouseover", mouseOver);
  document.getElementById("styledPara").addEventListener("mouseout", mouseOut);
 
  function mouseOver() 
  {
    document.getElementById("styledPara").style.color = "red";
    document.getElementById("styledPara").style.fontStyle = "Italic";
  }

  function mouseOut()
  {
    document.getElementById("styledPara").style.color = "black";
    document.getElementById("styledPara").style.fontStyle = "normal";
  }
}

